# Возможные темы докладов

## Теоретическая сессия

- **Опции коммерческих солверов**

- **Задача объединения (pooling problem)**

- **Последовательное линейное программирование**

- **Метод Вегштейна для решения системы нелинейных уравнений (для сведения рециклов)**

материалы

Knopf Modeling, Analysis and Optimization of Process and Energy Systems pp. 52-58

https://www.mathworks.com/matlabcentral/fileexchange/90337-wegstein-s-method-for-solving-non-linear-equations

- **Ограничения "alldifferent" и "allequal" в МП**

материалы 

https://yetanothermathprogrammingconsultant.blogspot.com/2008/10/magic-squares-in-gams.html


- **SOS переменные**

- **Частично целочисленные переменные**

материалы

https://www.msi-jp.com/xpress/learning/square/10-mipformref.pdf Partial integer variables


 - **Линеаризация нелинейных функций при помощи линейно кусочного представления**

материалы

 https://ampl.github.io/ampl-book.pdf с. 384

- **Open Source инструменты для IIS**

 материалы

 https://gekko.readthedocs.io/en/latest/global.html#coldstart

 https://github.com/pabloazurduy/python-mip-infeasibility

 http://www.sce.carleton.ca/faculty/chinneck/docs/GuieuChinneck.pdf

 https://pyomo.readthedocs.io/en/6.4.3/contributed_packages/iis.html

 https://www.gams.com/blog/2017/07/misbehaving-model-infeasible/

- **Автоматическая конвертация GAMS модели в LaTex. Утилита MODEL2TEX**

материалы 

https://www.gams.com/latest/docs/T_MODEL2TEX.html

- **Автоматическая конвертация MPS модели в GAMS модель**

материалы

https://www.gams.com/latest/docs/T_MPS2GMS.html

- **Обнаружение локального и глобального оптимума**

материалы

https://www.aspentech.com/ru/products/pages/aspen-pims-ao1

Avoid local optima—an inherent challenge in planning models—by utilizing PIMS-AO’s powerful multi-start capabilities and solution analysis tools. Have more confidence than ever that you have determined the absolute best solution for your refinery.

https://www.researchgate.net/publication/299144191_New_Optimization_Paradigms_for_Refinery_Planning
Varvarezos NEW OPTIMIZATION PARADIGMS FOR REFINERY PLANNING

https://discourse.julialang.org/t/best-way-to-improve-local-minimas-in-nonlinear-optimization/75215/4

When using gradient-based methods (such as Ipopt) on a nonconvex problem, the best you can do is multistart, as my colleagues suggested.

Alternatively, since your problem seems small, you can use interval methods: https://github.com/JuliaIntervals/IntervalOptimisation.jl 12
These are global methods that explore the whole search space and produce the global minimum. They’re significantly more expensive than local methods, but on a small problem, you can afford it.

https://www.researchgate.net/publication/358151220_Simplifying_deflation_for_non-convex_optimization_with_applications_in_Bayesian_inference_and_topology_optimization

- **Алгоритм решения job-shop problem Carlier-Pinson**

## Практическая сессия

- **макросы в GAMS**

материалы

https://www.gams.com/42/docs/UG_DollarControlOptions.html#UG_DollarControl_MacrosInGAMS

- **Форматы MPS файлов**

материалы

https://lpsolve.sourceforge.net/5.5/mps-format.htm

https://en.wikipedia.org/wiki/MPS_(format)

http://plato.asu.edu/cplex_mps.pdf

- *** Lazy constraints ***


 - **Warm Start в GAMS**

- **1334. Find the City With the Smallest Number of Neighbors at a Threshold Distance (Флойд Уоршелл)**
