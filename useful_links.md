# Полезные ссылки

## Учебные материалы

[Cornell University Computational Optimization Open Textbook](https://optimization.cbe.cornell.edu/index.php?title=Main_Page)

[Julia Programming for Operations Research 2/e by Changhyun Kwon](https://www.softcover.io/read/7b8eb7d0/juliabook2/introduction)

[Algorithms by Jeff Erickson](https://jeffe.cs.illinois.edu/teaching/algorithms/)

[Hands-On Optimization with Python](https://mobook.github.io/MO-book/intro.html#about-us)

[QuantEcon lectures on economics, finance, econometrics and data science](https://quantecon.org/lectures/)

[Математика для студентов МатФак ВШЭ](http://gorod.bogomolov-lab.ru/ps/stud/algebra-1/2324/list.html)

[Сборник рецептов по Pyomo(ND Pyomo Cookbook)](https://jckantor.github.io/ND-Pyomo-Cookbook/README.html)

[Optimization 101](https://www.optimization101.org/)

[JE Beasley's Tutorial questions and solutions](https://people.brunel.ac.uk/~mastjjb/jeb/or/tutorial.html)

## Блоги

[Alexander Kazachkov page](https://akazachk.github.io/seminars.html)

[Erwin Kalvelagen: Yet Another Math Programming Consultant](http://yetanothermathprogrammingconsultant.blogspot.com/)

[Paul A. Rubin: OR in an OB World](https://orinanobworld.blogspot.com/)

[Solver Max blog](https://www.solvermax.com/blog)

## Форумы

[Обзор основных методов математической оптимизации для задач с ограничениями(Habr)](https://habr.com/ru/articles/428794/)

[Математическая оптимизация и моделирование в PuLP(Habr)](https://habr.com/ru/articles/731006/)

[Математическое моделирование в ORtools(Habr)](https://habr.com/ru/articles/735572/)

[Constraint Programming или как решить задачу коммивояжёра, просто описав её(Habr)](https://habr.com/ru/articles/537492/)

[Метод генерации столбцов для решения задач математической оптимизации большой размерности(Habr)](https://habr.com/ru/articles/733804/)

[Stack Exchange: Operations Research](https://or.stackexchange.com/)

## Репозитории

[OptimizationExpert/Pyomo: много примеров на Pyomo в виде ipynb](https://github.com/OptimizationExpert/Pyomo/tree/main)