
Set
city
top
state / Alabama, Ohio, Arizona, Kansas, Texas/
bigCity(city<) /  Chicago, San-Francisco, Las-Vegas /
;
$onMulti
Table distance(city<, city<) 'distance in thousands of miles'
                Los-Angeles  San-Diego  Seattle  Vancouver
Boston              2986        3044      3046     3093
New-York            2790        2759      2852     2900
Atlanta             2187        2139      2638     2606
Orlando             2506        2429      3075     3043
;

Parameter
capital_city(state, city<) /
                            Alabama.Montgomery = 1,
                            Ohio.Columbus = 1,
                            Arizona.Phoenix = 1,
                            Kansas.Topeka = 1,
                            Texas.Austin = 1 /

population(city<, top<) 'mln' /
                            Chicago.3 = 2.75
                            San-Francisco.7 = 0.875
                            San-Diego.4 = 1.390
                            Boston.9 = 0.680
                            Austin.5 = 0.962
                            Atlanta.10 = 0.499
                            New-York.1 = 8.468
                            Seattle.8 = 0.737
                            Los-Angeles.2 = 3.899
                            Columbus.6 = 0.906
/
not_first_place(city, top)
first_place(city, top)
;

not_first_place(city, top) = population(city, top)$(ord(top)=1);
first_place(city, top) = population(city, top)$(sameas(top,"1"));

display city, top, not_first_place, first_place ;



