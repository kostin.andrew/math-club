*** Создание множеств из нескольких параметров
Set
city
bigCity(city<) /  Chicago, San-Francisco, Las-Vegas /
;
$onMulti
Parameter west_coast_city(city<) / Los-Angeles = 1, San-Diego = 1, Seattle = 1, Vancouver = 1, Portland = 1 /
;

Table distance(city<, city<) 'distance in thousands of miles'
                Los-Angeles  San-Diego  Seattle  Vancouver
Boston              2986        3044      3046     3093
New-York            2790        2759      2852     2900
Philadelphia        2711        2694      2822     2869
Atlanta             2187        2139      2638     2606
Orlando             2506        2429      3075     3043
;

display city;
