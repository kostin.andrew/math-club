*** Создание множеств из одного параметра
Set
city
west_coast
capital
east_coast
state / Alabama, Ohio, Arizona, Kansas, Texas/
bigCity(city<) /  Chicago, San-Francisco, Las-Vegas /
;

Table distance(east_coast<, west_coast<) 'distance in thousands of miles'
                Los-Angeles  San-Diego  Seattle  Vancouver
Boston              2986        3044      3046     3093
New-York            2790        2759      2852     2900
Philadelphia        2711        2694      2822     2869
Atlanta             2187        2139      2638     2606
Orlando             2506        2429      3075     3043
;

Parameter capital_city(state, capital<) /
Alabama.Montgomery = 1,
Ohio.Columbus = 1,
Arizona.Phoenix = 1,
Kansas.Topeka = 1,
Texas.Austin = 1 /
;



display city, west_coast, east_coast, capital, state;