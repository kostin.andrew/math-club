*** Создание множеств из одного параметра
Set
west_coast
west_city
east_coast
;

Parameter west_coast_city(west_city<) / Los-Angeles = 1, San-Diego = 0, Seattle = 1, Vancouver = 0, Portland = 1 /
;

Table distance(east_coast<, west_coast<) 'distance in thousands of miles'
                Los-Angeles  San-Diego  Seattle  Vancouver
Boston                         3044      3046     EPS
New-York                       2759      2852     EPS
Philadelphia                   2694      2822     EPS
Atlanta                        2139      2638     EPS
Orlando                        2429      3075     EPS
;

display west_city, west_coast, east_coast;