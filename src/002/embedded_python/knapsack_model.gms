$include .\knapsack_input.gms

free variables
objective;

positive variables
knapsack_weight;

binary variables
is_taken(item) "1 if item is taken to knapsack";

equations
knapsack_weight_calc
knapsack_weight_max
obj_calc
;

knapsack_weight_calc..
knapsack_weight =e= sum(item, is_taken(item) * weight(item));

knapsack_weight_max..
knapsack_weight =l= max_weight;

obj_calc..
objective =e= sum(item, is_taken(item) * cost(item));

* original model
model knapsack /knapsack_weight_calc, knapsack_weight_max, obj_calc/;

option optcr = 0.0;
option mip = cplex;

solve knapsack using mip maximizing objective;

display "no logic"
display objective.l, knapsack_weight.l, is_taken.l;


parameter result_taken(item);
result_taken(item) = is_taken.l(item);

embeddedCode Python:
import pandas as pd
taken = list(gams.get("result_taken"))
for i in taken:
     gams.printLog(f"Value of taken: {i}")
df = pd.DataFrame(taken, columns=["Item", "Is_taken"])
#df.to_excel("is_taken.xls")  # no module named xlwt
html = df.to_html()
file = open("is_taken.html", "w")
file.write(html)
file.close()
endEmbeddedCode

embeddedCode Python:
from openpyxl import Workbook
from openpyxl.styles import PatternFill
colored_cell =  PatternFill(start_color="FFFF00", end_color="FFFF00", fill_type="solid")
taken = list(gams.get("result_taken"))
taken.insert(0,("Item", "Is taken"))
wb = Workbook()
ws = wb.create_sheet("is_taken")
for row in taken:
     ws.append(row)
ws["A1"].fill = colored_cell
ws["B1"].fill = colored_cell
wb.save(filename="is_taken.xlsx")
endEmbeddedCode


* wrong environment
$onEmbeddedCode Python:
from openpyxl import Workbook
from openpyxl.styles import PatternFill
colored_cell =  PatternFill(start_color="FFFF00", end_color="FFFF00", fill_type="solid")
taken = list(gams.get("result_taken"))
taken.insert(0,("Item", "Is taken"))
wb = Workbook()
ws = wb.create_sheet("is_taken")
for row in taken:
     ws.append(row)
ws["A1"].fill = colored_cell
ws["B1"].fill = colored_cell
wb.save(filename="is_taken_02.xlsx")
$offEmbeddedCode









