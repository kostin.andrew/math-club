$onEmbeddedCode Python:
import sys
import os
pyVersion = str(sys.version_info.major) + '.' + str(sys.version_info.minor) + '.' + str(sys.version_info.micro)
gams.printLog(f"My Python version is {pyVersion}")
# list installed packeges without pip
a = help('modules')

from importlib.metadata import version

pandas_version = version("pandas")
numpy_version = version("numpy")
openpyxl_version = version("openpyxl")


gams.printLog(f"My Pandas version is {pandas_version}")
gams.printLog(f"My Numpy version is {numpy_version}")
gams.printLog(f"My Openpyxl version is {openpyxl_version}")


import json
gams.printLog(f"My Json version is {json.__version__}")

import sqlite3
gams.printLog(f"My Sqlite version is {sqlite3.version}")

import datetime
gams.printLog(f"My Datetime version is {datetime.__doc__}")
$offEmbeddedCode

