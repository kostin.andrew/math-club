set i /i1*i3/; set j /j1*j2/;
parameter g(i,j) /   i1.j1 = 10, i1.j2 = 0,
                     i2.j1 = 20, i2.j2 = EPS,
                     i3.j1 = 0.03, i3.j2 = 30/;
$onEmbeddedCode Python:
gams.epsAsZero  # without gams.epsAsZero same behaviour
py_gij = list(gams.get("g"))
gams.printLog("Elements of parameter g(i,j):")
for gij in py_gij:
     gams.printLog(f"Value of g(i,j): {gij}")
py_gij_flat = list(gams.get("g", keyFormat=KeyFormat.FLAT))
gams.printLog("Elements of parameter g(i,j) in FLAT format:")
for gij in py_gij_flat:
     gams.printLog(f"Value of g(i,j): {gij}")
$offEmbeddedCode
