set i;
parameter p(i);
$onEmbeddedCode Python:
i_elements = ["i" + str(i) for i in range(1,5)]
p_values = list(range(1,5))
p = list(zip(i_elements, p_values))
gams.set("i", i_elements)
gams.set("p", p)
$offEmbeddedCode i, p
display i, p;