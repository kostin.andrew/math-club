Set i /i1*i3/;
parameter p(i) /
i1 = 10,
i2 = 20,
i3 = 30
/;
$onEmbeddedCode Python:
gams_get_type = type(gams.get("i"))
gams.printLog(f"Type of gams.get {gams_get_type}")
py_i = list(gams.get("i"))
gams.printLog("Elements of set i:")
for i in py_i:
     gams.printLog(f"Type of i: {type(i)}, Value of i: {i}")
py_pi = list(gams.get("p"))
gams.printLog("Elements of parameter p(i):")
for pi in py_pi:
     gams.printLog(f"Type of pi: {type(pi)}, Value of p(i): {pi}")
$offEmbeddedCode
