POSITIVE VARIABLES
x_1
x_2
x_3
;
VARIABLES
obj
;

EQUATIONS
calc_objective
eq_min
eq_sum
eq_minus
;

calc_objective.. obj =e= 100*x_1 - 50*x_2 + 10*x_3;
eq_min.. x_3 =e= min(x_1, x_2);
eq_sum.. x_1 + 2*x_2 =l= 90;
eq_minus.. 3*x_1 - x_2 =g= 10;

MODEL example /all/;
SOLVE example using dnlp maximizing obj;

display obj.l, x_1.l, x_2.l, x_3.l;