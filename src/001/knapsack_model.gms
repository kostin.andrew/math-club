$include .\knapsack_input.gms

free variables
objective;

positive variables
knapsack_weight;

binary variables
is_taken(item) "1 if item is taken to knapsack";

equations
knapsack_weight_calc
knapsack_weight_max
obj_calc
;

knapsack_weight_calc..
knapsack_weight =e= sum(item, is_taken(item) * weight(item));

knapsack_weight_max..
knapsack_weight =l= max_weight;

obj_calc..
objective =e= sum(item, is_taken(item) * cost(item));

* original model
model knapsack /knapsack_weight_calc, knapsack_weight_max, obj_calc/;

option optcr = 0.0;
option mip = cplex;

solve knapsack using mip maximizing objective;

display "no logic"
display objective.l, knapsack_weight.l, is_taken.l;

equation and_constr "if gold taken then stone also taken";
and_constr..
is_taken("gold") =e= is_taken("stone");

model knapsack_and /knapsack_weight_calc, knapsack_weight_max, obj_calc, and_constr/;

solve knapsack_and using mip maximizing objective;
display "and logic"
display objective.l, knapsack_weight.l, is_taken.l;

equation if_constr "if gold taken then i32 also taken";
if_constr..
is_taken("gold") =l= is_taken("i32");

model knapsack_if /knapsack_weight_calc, knapsack_weight_max, obj_calc, if_constr/;
solve knapsack_if using mip maximizing objective;
display "if logic"
display objective.l, knapsack_weight.l, is_taken.l;


equation xor_constr "either gold or i20 taken, one of them or none ";
xor_constr..
is_taken("i15") + is_taken("i20") =l= 1;
model knapsack_xor /knapsack_weight_calc, knapsack_weight_max, obj_calc, xor_constr/;

solve knapsack_xor using mip maximizing objective;
display "xor logic"
display objective.l, knapsack_weight.l, is_taken.l;

scalar M;
M = sum(item, weight(item));
display M;

equation bigM01 " if gold taken, weight is less than 1000";

bigM01..
knapsack_weight =l= 1000 + M * (1 - is_taken("gold"));

equation bigM02 " if gold not taken, weight is greater than 1490";

bigM02..
knapsack_weight =g= 1490 - M * is_taken("gold");

model knapsack_ifelse /knapsack_weight_calc, knapsack_weight_max, obj_calc, bigM01, bigM02 /;
solve knapsack_ifelse using mip maximizing objective;
display "if else logic"
display objective.l, knapsack_weight.l, is_taken.l;

model superigra /knapsack_weight_calc, knapsack_weight_max, obj_calc, bigM01, bigM02, and_constr /;
solve superigra using mip maximizing objective;
display "superigra"
display objective.l, knapsack_weight.l, is_taken.l;








