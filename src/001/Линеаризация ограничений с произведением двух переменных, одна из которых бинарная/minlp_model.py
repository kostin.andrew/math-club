from pyomo.environ import *
import time
import logging
logging.getLogger('pyomo.core').setLevel(logging.ERROR)


# создание модели
model = ConcreteModel('New_model')

itm = list(range(1000))

weight_dict = {}
cost_dict = {}
bounds_compres = []
max_b = 10
min_b = 1.3

for i in itm:
    weight_dict[i] = 1.2 * i + 1
    cost_dict[i] = i + 1
    bounds_compres.append((min_b, max_b))

print('bounds_compres', bounds_compres)
print('weight_dict', weight_dict)
print('cost_dict', cost_dict)

model.item = Set(initialize=itm)  # номера предметов
model.weight = Param(model.item, initialize=weight_dict)  # вес предмета под номером из item
model.cost = Param(model.item, initialize=cost_dict)  # ценность предмета
model.total_weight = Param(initialize=20, doc='Total allowable weight')  # сколько вмещает рюкзак
model.take = Var(model.item, within=Binary)  # берем предмет или нет
model.compres = Var(model.item, within=Reals, bounds=bounds_compres)  # степень сжатия

# Ограничение на макс вес, учитывая компрессию предмета
def weight_constraint_rule(model):
    return sum(model.weight[i] * model.take[i] * model.compres[i] for i in model.item) <= model.total_weight

# Целевая функция
def objective_rule(model):
    return sum(model.cost[i] * model.take[i] for i in model.item)


model.weight_constraint = Constraint(rule=weight_constraint_rule)
model.objective_calc = Objective(rule=objective_rule, sense=maximize, doc='total cost(objective)')

solver = SolverFactory('scip', executable='C:\\Program Files\\SCIPOptSuite 8.0.3\\bin\\scip.exe')

start = time.time()
solver.solve(model, tee=False)

compres_opt = [model.compres[i].value for i in model.item]
take_opt = [model.take[i].value for i in model.item]
print('Total time:', time.time() - start)
print('Objective:', value(model.objective_calc))
print('compres', compres_opt)
print('take', take_opt)
print('***')

all_weight = 0
val = 0
for i in range(len(take_opt)):
    if take_opt[i] == 1.0:
        print(model.weight[i])
        val += 1
        all_weight += model.weight[i] * model.compres[i].value


print('Кол-во вещей:', val)
print('Общий вес:', all_weight)
print('Допустимый вес:', model.total_weight.value)


