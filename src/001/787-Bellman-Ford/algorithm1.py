class Solution:
    def findCheapestPrice(self, n: int, flights: List[List[int]], src: int, dst: int, k: int) -> int:

        # Записываем цены в удобную структуру
        graph : List[Dict[int, int]] = [{} for _ in range(n)]
        for _from, _to, price in flights:
            graph[_from][_to] = price
        
        # Инициализируем словарь со стоимостями перелетов
        cheapest_price = {src: 0}
        for s in range(k+1):
            cheapest_price_new = cheapest_price.copy()
            # Пробегаем по всем узлам в словаре
            for node1, price_to_node1 in cheapest_price.items():
                # Для каждого узла проверяем все смежные узлы
                for node2, price in graph[node1].items():
                    # Обновляем, если нашли более дешевую стоимость
                    cheapest_price_new[node2] = min(cheapest_price_new.get(node2, float('inf')), price_to_node1 + price)
            cheapest_price = cheapest_price_new
        
        return cheapest_price.get(dst, -1)
        
