class Solution:
    def findCheapestPrice(self, n: int, flights: List[List[int]], src: int, dst: int, k: int) -> int:

        # Записываем цены в удобную структуру
        graph : List[Dict[int, int]] = [{} for _ in range(n)]
        for _from, _to, price in flights:
            graph[_from][_to] = price
        
        # Инициализируем словарь со стоимостями перелетов
        cheapest_price = {src: 0}
        # Узлы, добавленные (обновленные) на предыдущем шаге
        prev_nodes = {src}
        for s in range(k+1):
            new_cheapest_price = cheapest_price.copy()
            # Узлы, пройденные на текущем шаге
            new_nodes = set()
            # Пробегаем по узлам, обновленным на предыдущем шаге
            for node1 in prev_nodes:
                price_to_node1 = cheapest_price[node1]
                # Проверяем все смежные узлы
                for node2, price in graph[node1].items():
                    # Обновляем, если нашли более дешевую стоимость
                    if node2 not in new_cheapest_price.keys() or new_cheapest_price[node2] > price_to_node1 + price:
                        new_cheapest_price[node2] = price_to_node1 + price
                        new_nodes.add(node2)

            cheapest_price = new_cheapest_price
            prev_nodes = new_nodes
        
        return cheapest_price.get(dst, -1)
