import collections
import heapq
from typing import List


class Solution:
    @staticmethod
    def networkDelayTime(times: List[List[int]], n: int, k: int) -> int:
        edges = collections.defaultdict(list)
        # добавляем к каждой вершине кортеж – куда идет связь и ее вес.
        for u, v, w in times:
            edges[u].append((v, w))

        minHeap = [(0, k)]
        # множество посещенных вершин
        visit = set()
        # результат посещения всех вершин
        t = 0
        while minHeap:
            # Извлекает и возвращает наименьший элемент из minHeap,
            # сохраняя инвариантность кучи.
            w1, n1 = heapq.heappop(minHeap)
            if n1 in visit:
                continue
            visit.add(n1)
            t = max(t, w1)

            for n2, w2 in edges[n1]:
                if n2 not in visit:
                    heapq.heappush(minHeap, (w1 + w2, n2))
        return t if len(visit) == n else -1


if __name__ == '__main__':
    times = [[1, 2, 3], [1, 3, 1], [1, 4, 3], [2, 3, 4], [3, 5, 7], [3, 6, 5], [4, 6, 2]]
    k = 1
    n = 6
    t = Solution.networkDelayTime(times, n, k)
    print(t)
