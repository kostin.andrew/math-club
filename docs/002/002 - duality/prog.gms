POSITIVE VARIABLES
x_1
x_2
x_3
x_4
;
VARIABLES
obj
;

EQUATIONS
calc_objective
eq_1
eq_2
eq_3
;

calc_objective.. obj =e= -27*x_1 - 10*x_2 - 15*x_3 - 28*x_4;
eq_1.. -3*x_1 - 2*x_2 - x_3 - 2*x_4 =g= -20;
eq_2.. -3*x_1 - x_2 - 3*x_3 - 4*x_4 =g= -50;
eq_3.. -2*x_1 - x_2 - x_3 - 2*x_4 =g= -60;

MODEL example /all/;
SOLVE example using mip minimizing obj;

display obj.l, x_1.l, x_2.l, x_3.l, x_4.l;
