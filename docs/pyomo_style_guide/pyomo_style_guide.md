<meta charset="utf-8">
<link rel="stylesheet" href=".css/markdown.css"></link>

<div class="document_title">Рекомендации по оформлению кода Pyomo-моделей</div>

Набор рекомендаций по оформлению кода *Pyomo*-моделей в отделе систем планирования и оптимизации А-С.

**ВЕРСИЯ**: 1.0

# Содержание
<a id = "toc" name = "toc"> </a>

- [Общее](#general)
    - [Комментирование в коде](#comments)
    - [Оформление docstring](#docstring)
    - [Используйте "аккуратные" данные (tidy data)](#use_tidy_data)
- [Pyomo код](#pyomo)
    - [Импорт Pyomo как pyo](#import)
    - [Иcпользуйте pyo.ConcreteModel](#concrete)
    - [Имена](#names)
    - [Индексация через множества pyomo](#use_set_for_index)
    - [Иcпользуйте domain вместо within](#use_domain)
    - [Иcпользуйте bounds, когда границы известны](#use_bounds)
    - [Используйте pyo.Constraint вместо pyo.ConstraintList](#use_constraint)
    - [Используйте декораторы](#use_decorators)
    - [Используйте quicksum](#use_quicksum)

- [Приложения](#appendices)
    - [Полезные ссылки](#links)

# Общее  [[TOC]](#toc)
<a id = "general" name = "general"></a>

## Комментирование в коде [[TOC]](#toc)
<a id = "comments" name = "comments"></a>

Комментарии нужны для сохранения знаний и эффективной отладки, когда над кодом работают несколько разработчиков. Комментарии должны быть реально нужны: комментировать очевидные вещи не нужно.

Комментарии должны быть короткими, но информативными. 

В комментариях **никогда** не нужно использовать грубые слова и мат. Юморить тоже не нужно. Это потом из-за недосмотра может увидеть заказчик.

В ходе **разработки** используйте общепринятые теги отладочных комментариев:

```python
#TODO: потом тут нужен рефакторинг
#FIXME: тут нужно что-то исправить
#XXX: обратите внимание на ...
#WARNING: тут может возникнуть проблема, если...
#ERROR: тут может возникнуть ошибка, если ...
```
Многие IDE подсвечивают такие теги или имеют расширения для такой подсветки.

В конечном продукте, передаваемом заказчику, отладочные комментарии с тегами стоит удалить.



## Оформление docstring [[TOC]](#toc)
<a id = "docstring" name = "docstring"></a>

Рекомендуется использовать формат [*Google*](https://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html) для оформления *docstring*.

Пример *docstring* в формате *Google*:

```python
"""Description of the function, class or method etc.

Args:
    varA (str): Description of varA
    varB (bool): Description of varB

Returns:
    list: Description of returned list

Raises:
    ValueError: Description of raised error
"""
```
В *Pyomo* сложная и многоуровневая структура подмодулей, что при попытке указать в *docstring* полноценное наименование типа аргумента или возвращаемого значения приводит к очень длинной строке с избыточной информацией. Поэтому для указания типа в докстринге рекомендуется использовать только непосредственный класс интересуемого объекта. Если уточнение о том, нужно ли именно *упорядоченное* множество (`OrderedScalarSet`) или *индексированный* параметр (`IndexedParam`) и тому подобное, несущественно, то можно ограничиться лаконичным указанием родительского класса (`Constraint`, `Param`, `Set` и пр.).

Типы часто используемых сущностей Pyomo для *docstring*:

| Сущность | Полное наименование типа | Короткое для докстринга |
|---|---|---|
| Двойственная оценка | pyomo.core.base.suffix.Suffix | Suffix |
| Множество | pyomo.core.base.set.OrderedScalarSet | OrderedScalarSet / Set|
| Модель | pyomo.core.base.PyomoModel.ConcreteModel | ConcreteModel |
| Ограничение | pyomo.core.base.constraint.IndexedConstraint | IndexedConstraint / Constraint |
| Параметр | pyomo.core.base.param.IndexedParam | IndexedParam / Param |
| Переменная | pyomo.core.base.var.Indexed Var | IndexedVar / Var |
| Решение | pyomo.opt.results.results_.SolverResults | SolverResults |
| Целевая функция | pyomo.core.base.objective.ScalarObjective | ScalarObjective / Objective |

## Используйте "аккуратные" данные (tidy data) [[TOC]](#toc)
<a id = "use_tidy_data" name = "use_tidy_data"></a>

# Pyomo код [[TOC]](#toc)
<a id = "pyomo" name = "pyomo"></a>

## Импорт pyomo как pyo [[TOC]](#toc)
<a id = "import" name = "import"></a>

Для подключении *Python*-библиотек через псевдоним (алиас) используется ключевое слово `as`. У многих популярных библиотек есть свой устоявшийся и привычный всем псевдоним. Для `numpy` это `np`, для `pandas` это `pd`, а для `pyomo.environ `- это `pyo`. Рекомендуется использовать именно псевдоним `pyo`, так как он используется и в официальной документации *Pyomo*, и в большинстве примеров кода на *Pyomo*. Не рекомендуется импортировать все классы и функции `pyomo.environ` без псевдонима через "звездочку" (`from module import * `) во избежание путаницы между стандартным типом `set `и одноименном *Pyomo*-классе `Set`.

<span style="color:green">Хорошо:</span>

```python
# общепринятый псевдоним pyo
import pyomo.environ as pyo


dpo_lp_model = pyo.ConcreteModel(name="DPO model")
```

<span style="color:red">Плохо:</span>

```python
# так будет коротко, но будет путаница для типа set и непонятно какой библиотеке 
# принадлежит класс или функция
from pyomo.environ import *


dpo_lp_model = ConcreteModel(name="DPO model")

# так будет очень длинно
import pyomo.environ


dpo_lp_model = pyomo.environ.ConcreteModel(name="DPO model") 

# так будет нетрадиционно
import pyomo.environ as pm


dpo_lp_model = pm.ConcreteModel(name="DPO model")
```


## Иcпользуйте pyo.ConcreteModel [[TOC]](#toc)
<a id = "concrete" name = "concrete"></a>

## Имена [[TOC]](#toc)
<a id = "names" name = "names"></a>

## Индексация через множества pyomo [[TOC]](#toc)
<a id = "use_set_for_index" name = "use_set_for_index"></a>

## Иcпользуйте domain вместо within [[TOC]](#toc)
<a id = "use_domain" name = "use_domain"></a>

## Иcпользуйте bounds, когда границы известны [[TOC]](#toc)
<a id = "use_bounds" name = "use_bounds"></a>

## Используйте pyo.Constraint вместо pyo.ConstraintList [[TOC]](#toc)
<a id = "use_constraint" name = "use_constraint"></a>

## Используйте декораторы [[TOC]](#toc)
<a id = "use_decorators" name = "use_decorators"></a>

Декораторы можно использовать, чтобы сделать код более читаемым. *Pyomo* позволяет использовать декораторы для создания следующих часто используемых компонентов оптимизационных моделей:

- параметров (`@model.Param(*)`);

- ограничений (`@model.Constraint(*)`);

- целевой функции (`@model.Objective(sense=*)`);

- дизъюнкций (`@model.Disjunction(*)`);

В общем виде декоратор такого вида:

```python
@<object>.<Component>(indices, ..., keywords=...)
def my_thing(m, index, ...):
    return #
```
позволяет заменить аналогичный код с `rule`:
```python
def my_thing_rule(m, index, ...):
    return #


<object>.my_thing = <Component>(indices, ..., rule=my_thing_rule, keywords=...)
```


<span style="color:red">Плохо:</span>

```python
# без декоратора приходится использовать избыточный rule. Нет докстринга - что 
# некритично, когда есть спецификация и критично, если нет никакой
# документации по модели
def new_constraint_rule(m, s):
    return m.x[s] <= m.ub[s]


m.new_constraint = pyo.Constraint(m.S, rule=new_constraint_rule)
```

<span style="color:green">Хорошо:</span>

```python
# c декоратором не приходится использовать rule, добавлен докстринг на случай
# отсутствия спецификации на модель
@m.Constraint(m.S)
def new_constraint_rule(m, s):
    """Функция new_constraint_rule создает верхнее ограничение на переменную x[s]
    в следующем виде:
    x[s] <= ub[s]

    Args:
        m (ConcreteModel): модель, куда включается это ограничение
        s (Set): множество S

    Returns:
        Constraint: верхнее ограничение на переменную x[s]

    """
    return m.x[s] <= m.ub[s]
```




## Используйте quicksum [[TOC]](#toc)
<a id = "use_quicksum" name = "use_quicksum"></a>

# Приложения [[TOC]](#toc)
<a id = "appendices" name = "appendices"></a>

## Полезные ссылки [[TOC]](#toc)
<a id = "links" name = "links"></a>

[Репозиторий Pyomo](https://github.com/Pyomo/pyomo)

[Pyomo Style Guide by Krzysztof Postek et al.](https://mobook.github.io/MO-book/notebooks/appendix/pyomo-style-guide.html)

[Data tidying: Подготовка наборов данных для анализа на конкретных примерах](https://habr.com/ru/articles/248741/)

[Сборник рецептов по Pyomo(ND Pyomo Cookbook)](https://jckantor.github.io/ND-Pyomo-Cookbook/README.html)

[Как использовать декораторы Pyomo в теле класса?](https://stackoverflow.com/questions/59070558/how-to-use-pyomo-decorator-within-a-class)

[Google Style Python Docstrings](https://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html)

[PEP 8 – Style Guide for Python Code](https://peps.python.org/pep-0008/)


