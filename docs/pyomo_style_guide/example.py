import pyomo.environ as pyo

product_list = ['P1', 'P2', 'P3', 'P4']

resource_list = ['R1', 'R2', 'R3']

norma_dict = {
    ('P1', 'R1'): 4, ('P1', 'R2') : 2,  ('P1', 'R3'): 1, ('P2', 'R1') : 2, ('P2', 'R2') : 10,
    ('P2', 'R3') : 0, ('P3', 'R1') : 2, ('P3', 'R2') : 6, ('P3', 'R3') : 2, ('P4', 'R1') : 8,
    ('P4', 'R2') : 0, ('P4', 'R3') : 1
}

price_dict =  { 
    'P1' : 65,
    'P2' : 70,
    'P3' : 60,
    'P4' : 120
}

resource_max_dict = {
    'R1' :  4800,
    'R2' :  2400,
    'R3' :  1500
}

cM = pyo.ConcreteModel(name='Concrete Model')

cM.product = pyo.Set(initialize=product_list, doc="Products")

cM.resource = pyo.Set(initialize=resource_list, doc="Resources")

cM.norma = pyo.Param(cM.product, cM.resource, initialize=norma_dict, doc="Norma of resource consumption")

cM.price = pyo.Param(cM.product, initialize=price_dict, doc="Prices of products")

cM.resource_max = pyo.Param(cM.resource, initialize=resource_max_dict, doc="Maximal Amount of Resourses")

cM.x = pyo.Var(cM.product, within=pyo.NonNegativeReals)

@cM.Constraint(cM.resource)
def upper_limit_resource(m, r):
    '''Функция upper_limit_resource создает верхнее ограничение на закупку
    ресурсов в следущем виде:
    SUM(product, norma[product,resource] * x[product]) <= recource_max[resource]

    Args:
        m (ConcreteModel): модель, куда включается это ограничение
        r (Set): множество ресурсов resource

    Returns:
        Constraint: верхнее ограничение на закупку ресурсов

    '''
    return pyo.quicksum(m.norma[p,r] * m.x[p] for p in m.product) <= m.resource_max[r]


@cM.Objective(sense=pyo.maximize)
def revenue(m):
    '''Функция revenue создает целевую функцию для максимизации выручки в
    следующем виде:
    revenue =  SUM(product, price[product] * x[product])

    Args:
        m (ConcreteModel): модель, куда включается эта целевая функция

    Returns:
        Objective: целевая функция выручки от продаж

    '''
    return pyo.quicksum(m.price[p] * m.x[p] for p in m.product)


solver = pyo.SolverFactory("appsi_highs")

assert solver.available(), f"Solver {solver} is not available."

cM.dual = pyo.Suffix(direction=pyo.Suffix.IMPORT_EXPORT)

cM.rc = pyo.Suffix(direction=pyo.Suffix.IMPORT_EXPORT)

result = solver.solve(cM, tee=False)

print("Тип значения кооторый возвращает solver.solve(): ", type(result))

print(f"Тип значения кооторый возвращает cM:", type(cM))

# print("***Concrete Model Display**")
# cM.display()
print("***Concrete Model PPRINT***")
cM.pprint()

# ConcreteModel не возвращает статистику если не вызвать метод compute_statistics()
print("Статистика модели")
cM.compute_statistics()
print("Общее число переменных:", cM.statistics.number_of_variables)
print("Общее число целевых функций:", cM.statistics.number_of_objectives)
print("Общее число ограничений:", cM.statistics.number_of_constraints)

# получить список переменных модели
print("Имена переменных модели c индексами")
indexed_variable_names = [v.getname() for v in cM.component_data_objects(pyo.Var)]
print(indexed_variable_names)

print("Имена переменных модели")
short_variable_names = []
for component_name, component in cM.component_map().items():
    if component.ctype is pyo.Var:
        short_variable_names.append(component_name)
print(short_variable_names)

print("Значение конкретной переменной модели по её строковому имени")
var_name = "x"
for component_name, component in cM.component_map().items():
    if component.ctype is pyo.Var and component_name == var_name:
        print(component.get_values())

print("Создаем словарь из всех переменных")
all_vars_dict = {}
for v in short_variable_names:
    for component_name, component in cM.component_map().items():
        if component.ctype is pyo.Var:
            all_vars_dict.update({v: component.get_values()}) 
print(all_vars_dict)

# после солва не будет работать

# print("Проверка на целочисленный тип")
# for v in short_variable_names:
#     for component_name, component in cM.component_map().items():
#         if component.ctype is pyo.Var:
#             if component.is_integer():
#                 print(f"Переменная {v} целочисленная")
#             else:
#                 print(f"Переменная {v} не целочисленная")

#print(cM.x.is_continous())

# записываем джейсон 
# json_object = json.dumps(all_vars_dict, indent=4)
# with open("ignore_me.json", "w") as outfile:
#     outfile.write(json_object)

# if (result.solver.status == pyo.SolverStatus.ok) and (result.solver.termination_condition == pyo.TerminationCondition.optimal):

#     # значение целевой функции

#     print(f'Objective is: {pyo.value(cM.revenue)}')

#     # значение конкретной переменной x
#     for s in cM.x:
#         print(f'for index {s} X[{s}] is: {cM.x[s].value}')

#     # двойственная оценка по ограничениям (Pi)
#     for s in cM.upper_limit_resource:
#         print(f'for index {s} dual of upper_limit_resource[{s}] is: {cM.upper_limit_resource[s].get_suffix_value(cM.dual)}')

#     # двойственная оценка по переменным (Dj)
#     for s in cM.x:
#         print(f'for index {s} reduced cost of x[{s}] is: {cM.x[s].get_suffix_value(cM.rc)}')

#     # значения всех переменных
#     for v in cM.component_data_objects(pyo.Var, active=True):
#         print(v, pyo.value(v), v.is_integer(), v.is_binary(),  v.is_continuous(), v.has_lb(), v.lb, v.has_ub(), v.ub, v.bounds)

#     # аттрибуты ограничений
#     for c in cM.component_data_objects(pyo.Constraint, active=True):
#         print(c.name, c.body, c.has_lb(), c.has_ub(), c.upper(), c.lslack(), c.uslack(), c.expr)

